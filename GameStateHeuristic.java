import java.util.ArrayList;
import java.util.Random;
import java.util.HashMap;
import java.util.Comparator;

public class GameStateHeuristic implements Comparator<GameState>{
    private HashMap<GameState, Integer> minLen;

    GameStateHeuristic(){
        minLen = new HashMap<>();
    }

    public void setMinValue(GameState node, int value){
        minLen.put(node, value);
    }

    public int getMinValue(GameState node){
        return minLen.get(node);
    }

    public int getValue(GameState node){
        int g = getMinValue(node);
        int h = getHeuristicValue(node);
        return g+h;
    }

    /**
     * heuristic is made up of 2 parts
     * 1. check for various ideal states e.g. have won, just got gold, etc.
     *    and return appropriate values
     * 2. if we haven't returned a value (we haven't reached any of the ideal
     *    states) we then get the minimum weighted manhattan distance for the
     *    nearest
     *    gold, axe, dynamite and unexplored area. at the same time we try to 
     *    restrict the use of dynamite
     * @return heuristic value of node
     */
    public int getHeuristicValue(GameState g){
		// Part 1: check for ideal states
		// if at terminal state then return 0
        if(g.getWon()){
            return 0;
        }
       
        // creates hashmap of nearest things e.g. nearest axe
        HashMap<Character, MapPoint> things = g.getThingLocations();

        char lastM = g.getLastMove();
        boolean hadGold = g.getPrevious().get_have_gold();
        boolean hadAxe = g.getPrevious().getHasAxe();

        // get gold
        if(!hadGold && g.get_have_gold()){
        	return 0;
        }
        // if we have gold then go home
        if(hadGold && g.get_have_gold()){
        	return g.startPoint().ManDist();
        }
        // prioritise cutting trees
        if(lastM == 'c') {
        	return 0;
        }
        // prioritise getting the axe
        if(!hadAxe && g.getHasAxe()){
        	return 1;
        }

        char infront = g.getPoint(0, -1).getType(); // char in front of agent
        // if haven't seen gold then it is good to explore
        if(!things.containsKey('g') && infront == '?'){
            return 0;
        }
        
        //
        // Part 2: get minimum weighted manhattan distance
        //

        int hVal = 0;
        int bestHval = Integer.MAX_VALUE;
 
        String toGet = "gad?"; // gold, axe, dynamite, unknown
        // weights for gold, axe, dynamite, unknown (gad?)
        int weights[] = {1,10,10,1}; 

        // if last move was use dynamite (blast) then:
        // increase unknown's weight (weight 3 = ?)
        if(lastM == 'b'){
            weights[3] *= 2;
        }

        // multiply manhattan distances of nearest gold, axe, dynamite and unknown
        // with previously defined weights and use the lowest 
        // value as the heuristic value
        for(int i = 0; i < toGet.length(); i++){
            char ch = toGet.charAt(i);
            if(things.containsKey(ch)){
                hVal = weights[i]*things.get(ch).ManDist();
                if(hVal < bestHval){
                    bestHval = hVal;
                }
            }
        }
        //devalues blasting for every case
        if(lastM == 'b'){
            bestHval *= 20;
        }

        return bestHval;
    }

    /**
     * A comparator for sorting the nodes by their heuristic value
     */
    public int compare(GameState o1, GameState o2) {
        Integer value1 = getValue(o1);
        Integer value2 = getValue(o2);

        if(value1 == null || value2 == null){
            return 0;
        }

        if(value1 > value2){
            return 1;
        }else if(value1 < value2){
            return -1;
        }
        return 0;
	}
}
