/**
 *  Agent.java 
 *  Based on the sample Agent for Text-Based Adventure Game
 *  Adam Dobrzycki and Joshua Pratt
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2015
 *
 *  This agent uses an A* search and a dynamic map (GameState) to
 *  simulate and evaluate possible worlds while building a world model
 *  to solve maps for the Bounty Game
 *  
 *  The GameSearch is an A* of GameStates using the GameStateHeuristic
 *  The GameStateHeuristic prioritizes worlds where the AI is closer to winning
 *  or has more resources
 *
 *  GameState uses a list of MapPoints to make a dynamic and extensible map
 *  which can be added to and expanded to any size
 *
 *  MapPoint: a class for vector points that supports rotation, 
 *  relative positions and manhattan distance
 */

import java.util.*;
import java.io.*;
import java.net.*;
import java.util.Random;

public class Agent {

	public static final boolean debugging = false;
	private GameState gameState;
	Random r = new Random();
	
	private String actionList = "";
	
	Agent(){
		gameState = new GameState();
	}
	
	public int getMoves(){
		return gameState.getMoves();
	}
	
	public char get_action(char view[][]) {
        gameState.setPrevious(gameState.clone());
        boolean updated = gameState.update(view);

		if(actionList.length() == 0){
			GameSearch stateSearch = new GameSearch(gameState);
			stateSearch.generateList();
			List<GameState> moves = stateSearch.getList();
			
			actionList = "";
            int lastNonTurn = 0;
            int index = 0;
			for(GameState state: moves){
                char l = state.getLastMove();
				actionList += l;
                if(l != 'l' && l != 'r'){
                    lastNonTurn = index;
                }
                index++;
			}
			actionList = actionList.substring(1, lastNonTurn+1);
		}
		
		if(actionList.length() == 0){
            actionList = "frflfr";
        }

		char action = actionList.charAt(0);
	    actionList = actionList.substring(1);
		
        String options = gameState.options();//check that the move is still legal
        
        if(options.indexOf(action) == -1){
            return get_action(view);//if so we mucked up somewhere...
        }
        
        gameState.tryMove(action);//this updates our knowledge of the game State
		
		if(Agent.debugging){
			gameState.draw();
            System.out.println("NUM TNTS "+gameState.getDynamitesHeld());
		}

		return action;
	}
	
	public static void main( String[] args ){
		InputStream in  = null;
		OutputStream out= null;
		Socket socket   = null;
		Agent  agent    = new Agent();
		int port;
		
		if( args.length < 2 ) {
			System.out.println("Usage: java Agent -p <port>\n");
			System.exit(-1);
		}
		
		port = Integer.parseInt( args[1] );
		
		try { // open socket to Game Engine
			socket = new Socket( "localhost", port );
			in  = socket.getInputStream();
			out = socket.getOutputStream();
		}catch( IOException e ) {
			System.out.println("Could not bind to port: "+port);
			System.exit(-1);
		}

		try { // scan 5-by-5 wintow around current location
			while( true ) {
				char   view[][] = new char[5][5];
				int i,j;
				for( i=0; i < 5; i++ ) {
					for( j=0; j < 5; j++ ) {
						if( !(( i == 2 )&&( j == 2 ))) {
							int ch = in.read();
							if( ch == -1 ) {
								if (Agent.debugging){
									System.out.println("Moves: "+agent.getMoves());
								}
								System.exit(-1);
							}
							view[i][j] = (char) ch;
						}
					}
				}
				//agent.print_view( view ); // COMMENT THIS OUT BEFORE SUBMISSION
				char action = agent.get_action( view );
				out.write( action );
				}
			}catch( IOException e ) {
				System.out.println("Lost connection to port: "+ port );
				System.exit(-1);
			}
		finally {
			try {
				socket.close();
			}catch( IOException e ) {
				e.printStackTrace();
			}
		}
	}
	
	void print_view( char view[][] ){
	}
}
