import java.util.LinkedList;
import java.util.HashMap;

public class GameState implements Cloneable{
    private GameState previous = null;
    private LinkedList<MapPoint> points;
    private MapPoint startPoint;
    private int moves = 0;
    
    private int dir = 0;
    
    private boolean have_axe  = false;
    private boolean have_key  = false;
    private boolean have_gold = false;
    private boolean in_boat   = false;
    
    private boolean game_won  = false;
    private boolean game_lost = false;
    private int num_dynamites_held = 0;

    private int destructions = 0;
    private boolean seenBoat = false;

    private char lastAction;
    
    GameState(){
        points = new LinkedList<MapPoint>();
        startPoint = new MapPoint(0, 0, 'H');
    }

    public void setPrevious(GameState prev){
        this.previous = prev;
    }

    public GameState getPrevious(){
        if (this.previous == null) {
            return this;
        } else {
            return this.previous;   
        }
    }

    public boolean isRepeat(){
        if(this.previous == null){
            return false;
        }
        return this.previous.repeatedState(this);
    }

    public boolean repeatedState(GameState other){
        if( this.dirEq(other, false) ){
            return true;
        }
        if( this.previous == null){
            return false;
        }
        return this.previous.repeatedState(other);
    }

    public boolean in_boat(){
        return in_boat;
    }
    
    public boolean getWon(){
        return game_won;
    }

    public boolean getGame_lost(){
        return game_lost;
    }
    
    public boolean get_have_gold(){
        return have_gold;
    }
    
    public MapPoint getStartPoint(){
        return startPoint;
    }

    public boolean getHasAxe(){
        return have_axe;
    }
    
    public char getLastMove(){
        return lastAction;
    }
    
    public int getDynamitesHeld() {
        return num_dynamites_held;
    }
    
    public boolean tryMove(char action){
        lastAction = action;
       
        updateUnknowns(getPoint(0,0));

        if(( action == 'L' )||( action == 'l' )) {
            rotateR();
            return( true );
        }
        
        if(( action == 'R' )||( action == 'r' )) {
            rotateL();
            return( true );
        }

        char ch = getPoint(0, -1).getType();//the place infront of the player

        if(( action == 'F' )||( action == 'f' )) {
            if(ch == '*' || ch == 'T' || ch == '-'){
                return( false );
            }
            if(ch == '~' && !in_boat){
                return( false );
            }
            translate(0, 1);
            if(" akgd".indexOf(ch) != -1){
                if( in_boat ){
                    updatePoint(new MapPoint(0,0, 'B'));
                    in_boat = false;
                }
            }

            switch( ch ) {
            case 'a': have_axe  = true;     break;
            case 'k': have_key  = true;     break;
            case 'g': have_gold = true;     break;
            case 'B': in_boat   = true;     break;
            case 'd': num_dynamites_held++; break;
            }
            if( have_gold &&( startPoint.getX() == 0 && startPoint.getY() == 0)) {
                   game_won = true;
            }

            return( true );
        }

        if(action =='C' || action == 'c'){ // chop
            if(( ch == 'T' )&& have_axe ) {
                destructions++;
                updatePoint(new MapPoint(0,0, ' '));
            }
            return( true );
        }

        if(action == 'O' || action == 'o'){ // open
             if(( ch == '-' )&& have_key ) {
                destructions++;
                updatePoint(new MapPoint(0,0, ' '));
             }
             return( true );
        }

        if(action == 'B' || action == 'b'){ // blast
            if(num_dynamites_held > 0){
                destructions++;
                updatePoint(new MapPoint(0,-1, ' '));
                num_dynamites_held--;
            }
            return( true );
        }

        return( false );
     }
    
    public String options(){
        if(game_lost){
            return "";
        }

        MapPoint p = getPoint(0, -1);//the place infront of the player
        String ops = "lr";//can always turn left or right (might not help though)

        char c = p.getType();
        if(c != '?' && c != 'T' && c != '*' && c != '.' && (in_boat || c != '~')){
            ops = 'f'+ops;//Move forward
        } else {    
            if(c == 'T' && have_axe){
                ops += 'c';
            }
    
            if(c == '-' && have_key){
                ops += 'o';
            }
   
            if(num_dynamites_held > 0){
                ops += 'b';
            }
        }
        
        return ops;
    }
            
    public boolean update(char view[][]){
        moves++;
        int i,j;
        boolean update = false;
        for(i=0; i < 5; i++ ) {
            for(j=0; j < 5; j++ ) {
                if(updatePoint(new MapPoint(i-2, j-2, view[j][i]))){
                    update = true;
                }
            }
        }
        return update;
    }

    public int getMoves(){
        return moves;
    }
    
    public void draw(){
        int x,y;

        int dirDiff = dir;

        while(dir > 0){
            rotateL();
        }

        int n = 15;
        for(y = -n; y < n; y++){
            String o = "";
            boolean gotR = false;
            int last = 0;
            for(x = -n; x < n; x++){
                char c = getPoint(x,y).getType();
                if(x == startPoint.getX() && y == startPoint.getY()){
                    c = 'H';
                }
                if(x == 0 && y == 0){
                    switch(dirDiff){
                        case 0: c = '^'; break;
                        case 1: c = '>'; break;
                        case 2: c = 'V'; break;
                        case 3: c = '<'; break;
                    }
                }
                if(c == 0){
                    c = ' ';
                }
                if(c != '.'){
                    gotR = true;
                }
                if(c == '.'){
                    c = ' ';
                }

                if(c != ' '){
                    last = x;
                }
                o += c;
            }
            if(gotR){
                o = o.substring(0, last+n+1);
                System.out.println(o);
            }
        }
        System.out.println("\n");

        while(dir != dirDiff){
            rotateL();
        }
    }

    public boolean updatePoint(MapPoint newPoint){
        //updates a point in the GameState
        if(newPoint.getType() == 'B'){
            seenBoat = true;
        }

        boolean update = false;
        MapPoint r = null;
        for(MapPoint p : points){
            if(p.getX() == newPoint.getX() && p.getY() == newPoint.getY()){
                r = p;
                break;
            }
        }
        update = (r == null);
        if(r != null && newPoint.getType() != '?'){
            points.remove(r);
            update = true;
            updateUnknowns(r);
        }
        if(update){
            points.add(newPoint);
        }
        return update;
    }

    public void updateUnknowns(MapPoint n){
        char ch = n.getType();
        if(ch != '.' && ch != '?' && (seenBoat || ch != '~')){
            updatePoint(new MapPoint(n.getX()-1, n.getY(), '?'));//left
            updatePoint(new MapPoint(n.getX()+1, n.getY(), '?'));//right
            updatePoint(new MapPoint(n.getX(), n.getY()-1, '?'));//up
            updatePoint(new MapPoint(n.getX(), n.getY()+1, '?'));//down
        }
    }

    public MapPoint getPoint(int x, int y){
        for(MapPoint p : points){
            if(p.getX() == x && p.getY() == y){
                return p;
            }
        }
        return new MapPoint(x, y, '.');
    }
    
    public MapPoint startPoint(){
        return startPoint;
    }
    
    // get hashmap of nearest locations for each thing
    public HashMap<Character, MapPoint> getThingLocations() {
        HashMap<Character, MapPoint> result = new HashMap<Character, MapPoint>();
        for(MapPoint p : points){
            MapPoint nearest = result.get(p.getType());
            if(nearest == null || p.ManDist() < nearest.ManDist()){ 
                result.put(p.getType(), p);
            }
        }
        return result;
    }

    public void rotateL(){//rotate the map left
        dir = (dir+1)%4;
        startPoint.rotateL();

        for(MapPoint p : points){
            p.rotateL();
        }
    }

    public void rotateR(){//rotate the map right
        dir = (dir+3)%4;
        startPoint.rotateR();
        for(MapPoint p : points){
            p.rotateR();
        }
    }

    public void translate(int dx, int dy){//translate all the points
        startPoint.translate(dx, dy);
        
        for(MapPoint p : points){
            p.translate(dx, dy);
        }
    }
    
    public GameState clone(){
        GameState n = new GameState();
        //Objects
        n.points = new LinkedList<MapPoint>();
        for(MapPoint p : this.points){
            n.points.add((MapPoint)p.clone());
        }
        n.dir = this.dir;
        n.startPoint = (MapPoint)this.startPoint.clone();
        //Basic types
        n.moves = this.moves;
        n.in_boat = this.in_boat;
        n.have_axe = this.have_axe;
        n.have_key = this.have_key;
        n.have_gold = this.have_gold;
        n.game_won = this.game_won;
        n.game_lost = this.game_lost;
        n.num_dynamites_held = this.num_dynamites_held;
        n.lastAction = this.lastAction;       
        n.previous = this.previous;
        n.destructions = this.destructions;
        n.seenBoat = this.seenBoat;

        return n;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + lastAction;
        result = prime * result + (game_lost ? 1231 : 1237);
        result = prime * result + (game_won ? 1231 : 1237);
        result = prime * result + (have_axe ? 1231 : 1237);
        result = prime * result + (have_gold ? 1231 : 1237);
        result = prime * result + (have_key ? 1231 : 1237);
        result = prime * result + num_dynamites_held;
        result = prime * result + destructions;

        result = prime * result + startPoint.getX();
        result = prime * result + startPoint.getY();
        result = prime * result + dir;
        return result;
    }

    @Override
    public boolean equals(Object obj){
        return dirEq(obj, true);
    }

    public boolean dirEq(Object obj, boolean directed) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameState other = (GameState) obj;
        if (game_lost != other.game_lost)
            return false;
        if (game_won != other.game_won)
            return false;
        if (have_axe != other.have_axe)
            return false;
        if (have_gold != other.have_gold)
            return false;
        if (have_key != other.have_key)
            return false;
        if (directed && destructions != other.destructions)
            return false;
        if ((lastAction == 'c') != (other.lastAction == 'c'))
            return false;
        if ((lastAction == 'b') != (other.lastAction == 'b'))
            return false;
        
        if (num_dynamites_held != other.num_dynamites_held)
            return false;
        if(directed && dir != other.dir){
            return false;
        }

        MapPoint start = startPoint.clone();
        for(int d = dir; d != other.dir; d = (d+1)%4){
            start.rotateL();
        }

        if (!start.equals(other.startPoint)){
            return false;
        }
        return true;
    }
}
