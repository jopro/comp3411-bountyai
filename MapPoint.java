
public class MapPoint implements Cloneable{
	private int x;
	private int y;
	private char type;
	
	MapPoint(int x, int y, char type){
		this.x = x;
		this.y = y;
		this.type = type;
	}
	
	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}
	
	public int ManDist(){
		return Math.abs(getX()) + Math.abs(getY());
	}
	
	public char getType(){
		return type;
	}
	
	public void setType(char type){
		this.type = type;
	}
	
	public void rotateL(){
		int lx = x;
		int ly = y;
		
		x = ly;
		y = -lx;
	}
	
	public void rotateR(){
		int lx = x;
		int ly = y;
		
		x = -ly;
		y = lx;
	}
	
	public void translate(int dx, int dy){
		x += dx;
		y += dy;
	}
	
	public MapPoint clone(){
		return new MapPoint(this.x, this.y, this.type);
	}
	
	public boolean equals(Object other){
		if(other != null && other.getClass() == this.getClass()){
			MapPoint o = (MapPoint) other;
			if(o.getX() == getX() && o.getY() == getY() && o.getType() == getType()){
				return true;
			}
		}
		return false;
	}
}