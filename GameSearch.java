import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class GameSearch{
	private GameStateHeuristic heuristic;
	private LinkedList<GameState> list;
	
	private GameState start;
	int numExpanded;
	int maxNodes = 6001;
	/**
	 * Generates an A* search path generator
	 * @param graph the graph to search through
	 * @param start the node to start at
	 * @param heuristic the heuristic used to search through the graph
	 */
	GameSearch(GameState start){
		this.start = start;
		this.heuristic = new GameStateHeuristic();
		this.list = null;
	}
	
	public void setMax(int max){
		maxNodes = max;
	}
	
	public void generateList() {
		numExpanded = 0;
		list = new LinkedList<>();
		PriorityQueue<GameState> frontier = new PriorityQueue<>(1, heuristic);
		HashMap<GameState, GameState> pre = new HashMap<>();
	    
	    pre.put(start, null);
	    heuristic.setMinValue(start, 0);
	   
	    GameState bestNode = null;
	    int bestValue = 0;
 
	    GameState curr = start;
	    while(curr != null && numExpanded < maxNodes){
	    	int f = heuristic.getHeuristicValue(curr);

		if(bestNode == null || bestValue > f){
			bestNode = curr;
			bestValue = f;
        	list = reconstruct(pre, bestNode);
			if(f == 0){
				break;
			}
		}
	        
	    int distanceTraveled = heuristic.getMinValue(curr);
		for(GameState node : getNexts(curr)){
			boolean notAlreadyVisited = pre.get(node) == null;
			int pathLen = distanceTraveled + 1;//distance traveled along the path
		
			if(notAlreadyVisited || pathLen < heuristic.getMinValue(node)){
				heuristic.setMinValue(node, pathLen);
				pre.put(node, curr);//got here from curr
	                	
	            		if(notAlreadyVisited){
	                		frontier.add(node);//expand this node
	                	}
	            	}
    	    	}
	    	curr = frontier.poll();//searching for minimal F score
	    	numExpanded += 1;
	    }
	}
	
	public int getNumExpanded() {
		return numExpanded;
	}

	/**
	 * Reconstructs the path from the 'pre' hashmap
	 * @param pre a hashmap of nodes and their previous nodes
	 * @param curr the current node from which to reconstruct from
	 * @return the list of nodes passed on the way to the current node
	 */
	private LinkedList<GameState> reconstruct(HashMap<GameState, GameState> pre, GameState curr){
	    LinkedList<GameState> foundPath = new LinkedList<>();
	    while(curr != start && curr != null){
	        foundPath.addFirst(curr);
	        curr = pre.get(curr);
	    }
        foundPath.addFirst(curr);
	    return foundPath;
	}
	
	public LinkedList<GameState> getList() {
		return list;
	}

    public LinkedList<GameState> getNexts(GameState curr){
        LinkedList<GameState> nexts = new LinkedList<>();
        String ops = curr.options();
        
        for(int index = 0; index < ops.length(); index++){
            char action = ops.charAt(index);
            GameState newS = curr.clone();
            newS.tryMove(action);
            newS.setPrevious(curr);
            
            if(!newS.getGame_lost()){
                nexts.add(newS);
            }
        }
        return nexts;
    }
}
